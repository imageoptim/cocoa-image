//! This is based on [ImgRef](https://lib.rs/crates/rgb) and [RGB](https://lib.rs/crates/rgb) crates.

pub use rgb::RGBA8;

mod error;
pub use crate::error::Error;
#[cfg(target_os = "macos")] mod macos;
#[cfg(target_os = "macos")] pub use crate::macos::*;

/// On macOS returns an RGBA 8-bit bitmap in sRGB color space with premultiplied alpha.
///
/// The input is an encoded file (PNG, JPEG, etc.)
///
/// See [`ImgRef`](https://lib.rs/crates/rgb) for instructions how to use the returned bitmap.
#[cfg(not(target_os = "macos"))]
pub fn decode_image_as_rgba_premultiplied(_file_data: &[u8]) -> Result<imgref::ImgVec<RGBA8>, Error> {
    Err(Error::Unsupported)
}

#[cfg(not(target_os = "macos"))]
/// On macOS returns an RGBA 8-bit bitmap in sRGB color space with regular (non-associated) alpha.
pub fn decode_image_as_rgba(_file_data: &[u8]) -> Result<imgref::ImgVec<RGBA8>, Error> {
    Err(Error::Unsupported)
}

#[test]
fn poke() {
    let _ = decode_image_as_rgba_premultiplied(&[]);
}
