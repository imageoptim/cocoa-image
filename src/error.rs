use std::fmt;

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Error {
    /// Wrong platform
    Unsupported,
    /// Misc ObjC setup failed (should not happen)
    ObjcError,
    /// Image is probably invalid/corrupted/wrong format
    DecodeError,
}

impl std::error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(match self {
            Self::Unsupported => "Wrong platform",
            Self::ObjcError => "Misc ObjC setup failed (should not happen)",
            Self::DecodeError => "Image is probably invalid/corrupted/wrong format",
        })
    }
}
