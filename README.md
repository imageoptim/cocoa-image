# Load image in [Rust](https://www.rust-lang.org) using [Cocoa](https://lib.rs/crates/core-graphics) decoders

Allows reading any image format that macOS supports natively, in Rust.

Returns an [RGBA](https://lib.rs/crates/rgb) 8-bit bitmap in sRGB color space with premultiplied alpha.

Links with `AppKit.framework`. Compiles to nothing on other platforms (just gracefully always returns an error).

